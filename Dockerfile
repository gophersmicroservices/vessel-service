FROM golang:1.11 as builder
WORKDIR /app/vessel-service
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o bin/vessel-service

FROM alpine:latest

WORKDIR /app
COPY --from=builder /app/vessel-service/bin/vessel-service .
CMD ["./vessel-service"]