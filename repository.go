package main

import (
	pb "bitbucket.org/gophersmicroservices/vessel-service/proto/vessel"
	 "golang.org/x/net/context"

	"github.com/mongodb/mongo-go-driver/bson"
	"github.com/mongodb/mongo-go-driver/bson/primitive"
	"github.com/mongodb/mongo-go-driver/mongo"
)

type VesselDocument struct {
	ID primitive.ObjectID `bson:"_id"`
	OwnerID primitive.ObjectID `bson:"owner_id"`
	Capacity int32 `bson:"capacity"`
	Maxweight int32 `bson:"max_weight"`
	Available bool `bson:"available"`
	Name string `bson:"name"`
}

func NewVesselDocument(v *pb.Vessel) *VesselDocument {
	ownerID, _ := primitive.ObjectIDFromHex(v.OwnerId)

	return &VesselDocument{
		ID: primitive.NewObjectID(),
		OwnerID:   ownerID,
		Capacity:  v.Capacity,
		Maxweight: v.MaxWeight,
		Available: v.Available,
		Name: v.Name,
	}
}

func (d *VesselDocument) ToVessel() *pb.Vessel {
	return &pb.Vessel{
		Id:                   d.ID.Hex(),
		Capacity:             d.Capacity,
		MaxWeight:            d.Maxweight,
		Name:                 d.Name,
		Available:            d.Available,
		OwnerId:              d.OwnerID.Hex(),
	}
}

type VesselRepository struct {
	c *mongo.Collection
}

func NewRepository(client *mongo.Client) *VesselRepository {
	return &VesselRepository{
		c: client.Database("shippy").Collection("vessels"),
	}
}

func (repo *VesselRepository) Create(vessel *pb.Vessel) error {
	_, err := repo.c.InsertOne(context.Background(), NewVesselDocument(vessel))
	return err
}

func (repo *VesselRepository) FindAvailable(spec *pb.Specification) (*pb.Vessel, error) {
	var doc *VesselDocument
	q := bson.M{
		"capacity":   bson.M{"$gte": spec.Capacity},
		"max_weight": bson.M{"$gte": spec.MaxWeight},
	}

	if err:=repo.c.FindOne(context.Background(), q).Decode(&doc); err != nil {
		return nil, err
	}

	return doc.ToVessel(), nil
}

