build:
	protoc --go_out=plugins=micro:. proto/vessel/vessel.proto
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o bin/vessel-service

run:
	docker build -t vessel-service .
	docker run -p 50052:50051 -e MICRO_SERVER_ADDRESS=:50051 -e MICRO_REGISTRY=consul vessel-service