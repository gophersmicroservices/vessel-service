package main

import (
	pb "bitbucket.org/gophersmicroservices/vessel-service/proto/vessel"
	"golang.org/x/net/context"

	"fmt"
	"github.com/micro/cli"
	"github.com/micro/go-micro"
	"github.com/mongodb/mongo-go-driver/mongo"
	"log"
	"time"
)
type Config struct {
	MongoDSN string
}

func main() {
	var config Config
	srv := micro.NewService(
		micro.Name("go.micro.srv.vessel"),
		micro.Version("latest"),
		micro.Flags(cli.StringFlag{
			Name:        "mongo_dsn",
			Value:       "mongodb://localhost:27017",
			Usage:       "MongoDB DSN",
			EnvVar:      "MONGO_DSN",
			Destination: &config.MongoDSN,
		}),
	)
	srv.Init()

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, config.MongoDSN)
	if err != nil {
		log.Fatalln(err)
	}

	repo := NewRepository(client)
	pb.RegisterVesselServiceHandler(srv.Server(), &service{repo})

	if err := srv.Run(); err != nil {
		fmt.Println(err)
	}
}
