module bitbucket.org/gophersmicroservices/vessel-service

require (
	github.com/go-log/log v0.1.0 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/golang/protobuf v1.2.0
	github.com/golang/snappy v0.0.0-20180518054509-2e65f85255db // indirect
	github.com/google/uuid v1.1.0 // indirect
	github.com/hashicorp/consul v1.4.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.0 // indirect
	github.com/hashicorp/go-rootcerts v0.0.0-20160503143440-6bb64b370b90 // indirect
	github.com/hashicorp/serf v0.8.1 // indirect
	github.com/micro/cli v0.0.0-20180830071301-8b9d33ec2f19
	github.com/micro/go-log v0.0.0-20170512141327-cbfa9447f9b6 // indirect
	github.com/micro/go-micro v0.14.1
	github.com/micro/go-rcache v0.0.0-20180418165751-a581a57b5133 // indirect
	github.com/micro/h2c v1.0.0 // indirect
	github.com/micro/mdns v0.0.0-20181201230301-9c3770d4057a // indirect
	github.com/micro/util v0.0.0-20181115195001-2d4f591dc538 // indirect
	github.com/miekg/dns v1.1.1 // indirect
	github.com/mitchellh/hashstructure v1.0.0 // indirect
	github.com/mitchellh/mapstructure v1.1.2 // indirect
	github.com/mongodb/mongo-go-driver v0.1.0
	github.com/pkg/errors v0.8.0 // indirect
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20181203042331-505ab145d0a9 // indirect
	golang.org/x/net v0.0.0-20181207154023-610586996380
	golang.org/x/sync v0.0.0-20181108010431-42b317875d0f // indirect
	golang.org/x/sys v0.0.0-20181211161752-7da8ea5c8182 // indirect
	golang.org/x/text v0.3.0 // indirect
)
