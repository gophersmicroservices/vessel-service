package main

import (
	pb "bitbucket.org/gophersmicroservices/vessel-service/proto/vessel"
	"golang.org/x/net/context"

)

type Repository interface {
	FindAvailable(*pb.Specification) (*pb.Vessel, error)
	Create(vessel *pb.Vessel) error
}

type service struct {
	repo Repository
}

func (s *service) FindAvailable(ctx context.Context, req *pb.Specification, res *pb.Response) error {
	vessel, err := s.repo.FindAvailable(req)
	if err != nil {
		return err
	}

	res.Vessel = vessel
	return nil
}

func (s *service) Create(ctx context.Context, req *pb.Vessel, res *pb.Response) error {
	err := s.repo.Create(req)
	if err != nil {
		return err
	}

	res.Vessel = req
	res.Created = true
	return nil
}
